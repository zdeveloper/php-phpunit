<?php

/**
 * Class AbstractEntityTest.
 */
abstract class AbstractEntityTest extends \PHPUnit_Framework_TestCase
{
    abstract public function entityClass();

    public function testClassExist()
    {
        $this->assertTrue(class_exists(get_class($this->entityClass())));
        //$this->assertInstanceOf('CIANDT\BaseBundle\Entity\AbstractEntity', $this->entityClass());
    }

    /**
     * @return array
     */
    abstract public function dataProvider();

    /**
     * @dataProvider dataProvider
     */
    public function testCheckGetAndSetExpected($attribute, $value)
    {

        $get = 'get'.str_replace(' ', '', ucwords(str_replace('_', ' ', $attribute)));
        $set = 'set'.str_replace(' ', '', ucwords(str_replace('_', ' ', $attribute)));

        $class = $this->entityClass();
        $this->assertNull($class->$get());

        $class->$set($value);

        $this->assertNotNull($class->$get());
        $this->assertEquals($value, $class->$get());

    }

    /**
     * @dataProvider dataProvider
     */
    public function testCheckMethodsFluid($attribute, $value)
    {
        $set = 'set'.str_replace(' ', '', ucwords(str_replace('_', ' ', $attribute)));
        $this->assertNotNull($set);
        $class = $this->entityClass();
        $result = $class->$set($value);

        $this->assertClassHasAttribute($attribute, get_class($class));
        $this->assertInstanceOf(get_class($this->entityClass()), $result);
    }


    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage setId accept only positive integers greater than zero and
     */
    public function testReturnsExceptionIfNotAnIntegerParameter()
    {
        $class = $this->entityClass();
        for ($i = 0; $i <= 2; ++$i) {
            switch ($i) {
                case 0:
                    $class->setId('hello');
                    break;
                case 1:
                    $class->setId(-1);
                    break;
                case 2:
                    $class->setId(0);
                    break;
            }
        }
    }

    protected function mockCollection()
    {
        return $this->getMockBuilder('Doctrine\Common\Collections\ArrayCollection')
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        $refl = new \ReflectionObject($this);
        foreach ($refl->getProperties() as $prop) {
            if (!$prop->isStatic() && 0 !== strpos($prop->getDeclaringClass()->getName(), 'PHPUnit_')) {
                $prop->setAccessible(true);
                $prop->setValue($this, null);
            }
        }
        parent::tearDown();
    }

}

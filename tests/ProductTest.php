<?php

/**
 * Created by PhpStorm.
 * User: diego
 * Date: 05/01/17
 * Time: 20:07
 */
class ProductTest extends AbstractEntityTest
{
    /**
     * {@inheritdoc}
     */
    public function entityClass()
    {

        $pdo = $this->getMockBuilder(\PDO::class)
            ->disableOriginalConstructor()->getMock();

        return new \SON\Model\Product($pdo);
    }


    /**
     * {@inheritdoc}
     */
    public function dataProvider()
    {
        return [
            ['id', 1],
            ['name', 'user_test'],
            ['price', 2],
            ['quantity', 12],
            ['total', 10],
        ];
    }


}